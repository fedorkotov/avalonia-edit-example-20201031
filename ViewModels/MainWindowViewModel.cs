﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvaloniaEditTest.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Hello World!";
    }
}
